<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix'=>'wallet'], function (){
   Route::post('/',[\App\Http\Controllers\WalletController::class, 'enableWallet']);
   Route::get('/',[\App\Http\Controllers\WalletController::class, 'showWallet']);
   Route::patch('/',[\App\Http\Controllers\WalletController::class, 'disableWallet']);
   Route::get('/transactions',[\App\Http\Controllers\Transaction::class,'listTransaction']);
   Route::post('/deposits',[\App\Http\Controllers\Transaction::class, 'topup']);
   Route::post('/withdraws',[\App\Http\Controllers\Transaction::class, 'withdraw']);

});
Route::post('/init',[\App\Http\Controllers\WalletController::class, 'initWallet']);


