<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class PaginationResource extends JsonResource
{
    private mixed $modelResource;
    private mixed $type;

    public function __construct(LengthAwarePaginator $resource, mixed $modelResource, string $type = null)
    {
        $this->modelResource = $modelResource;
        $this->type = $type;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        /** @var LengthAwarePaginator $paginationData */
        $paginationData = $this->resource;
        $currentPage = $paginationData->currentPage();
        $lastPage = $paginationData->lastPage();
        if (is_string($this->modelResource)) {
            $data = $paginationData->getCollection()
                ->map(fn($item) => new $this->modelResource($item));
        } elseif (is_object($this->modelResource)) {
            $data = $this->modelResource;
        } else {
            $data = null;
        }
        if ($this->type == 'singular') {
            $data = $data->first();
        }

        return [
            'currentPage' => $currentPage,
            'data' => $data,
            'firstPageUrl' => $paginationData->url(1),
            'lastPage' => $lastPage,
            'lastPageUrl' => $paginationData->url($lastPage),
            'nextPageUrl' => $paginationData->nextPageUrl(),
            'perPage' => $paginationData->perPage(),
            'prevPageUrl' => $paginationData->previousPageUrl(),
            'total' => $paginationData->total(),
            'hasNextPage' => $paginationData->hasMorePages()
        ];
    }
}
