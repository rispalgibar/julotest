<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    public function auth($token):mixed
    {
        $user = User::where('id', md5($token))->first();
        if (!$user){
            return response()->json([
                'data'=>[
                    'error'=>[
                        'cutomer_xid'=>['Not authenticate.']
                    ]
                ],
                'status'=>'failed'
            ], Response::HTTP_UNAUTHORIZED);
        }
        return $user;
    }
}
