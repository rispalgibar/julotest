<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Transaction extends Controller
{
    public function listTransaction($token):JsonResponse
    {
        $user = $this->auth($token);
        self::findWallet($user->id);
        $transactions = \App\Models\Transaction::where(['deposited_by'=>$user->id, 'withdrawn_by'=>$user->id])->get();
        return response()->json([
            'data' => [
                'transactions'=>$transactions
            ],
            'status'=> 'success'
        ], Response::HTTP_CREATED);

        
    }

    public function topup($token, $amount, $reference_id):JsonResponse
    {
        $user = $this->auth($token);
        if ($amount < 0){
            return response()->json([
                'data' => [
                    'error' => [
                        'Invalid Amount.'
                    ]
                ],
                'status' => 'fail'
            ], Response::HTTP_BAD_REQUEST);
        }

        $wallet = self::findWallet($user->id);
        $wallet->update(['balance', $wallet->balance + $amount]);
        $transaction = \App\Models\Transaction::create([
            'status'=>'deposit',
            'transacted_at'=> Carbon::now(),
            'deposited_at'=> Carbon::now(),
            'amount'=>$amount,
            'reference'=> $reference_id,
        ]);
        return response()->json([
            'data' => [
                'transactions'=>$transaction
            ],
            'status'=> 'success'
        ], Response::HTTP_CREATED);

    }

    public function withdraw($token, $amount, $reference_id):JsonResponse
    {
        $user = $this->auth($token);
        if ($amount < 0){
            return response()->json([
                'data' => [
                    'error' => [
                        'Invalid Amount.'
                    ]
                ],
                'status' => 'fail'
            ], Response::HTTP_BAD_REQUEST);
        }

        $wallet = self::findWallet($user->id);
        $wallet->update(['balance', $wallet->balance - $amount]);
        $transaction = \App\Models\Transaction::create([
            'status'=>'deposit',
            'transacted_at'=> Carbon::now(),
            'deposited_at'=> Carbon::now(),
            'amount'=>$amount,
            'reference'=> $reference_id,
        ]);
        return response()->json([
            'data' => [
                'transactions'=>$transaction
            ],
            'status'=> 'success'
        ], Response::HTTP_CREATED);

    }

    public static function findWallet(string $uid):mixed
    {
        $wallet = Wallet::where('owned_by',$uid)->first();
        if ($wallet){
            return response()->json([
                'data' => [
                    'error' => [
                        'Wallet notfound.'
                    ]
                ],
                'status' => 'fail'
            ], Response::HTTP_BAD_REQUEST);

        }elseif (!$wallet->status) {
            return response()->json([
                'data' => [
                    'error' => [
                        'Wallet disabled.'
                    ]
                ],
                'status' => 'fail'
            ], Response::HTTP_BAD_REQUEST);
        }

        return $wallet;
    }
}