<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->boolean('status')->default(false);
            $table->timestamp('transacted_at');
            $table->string('type');
            $table->integer('amount')->default(0);
            $table->uuid('reference');
            $table->foreignUuid('withdrawn_by')->references('id')->on('users');
            $table->foreignUuid('deposited_by')->references('id')->on('users');
            $table->timestamp('withdrawn_at');
            $table->timestamp('deposited_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaction');
    }
};
