<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
class WalletController extends Controller
{
    public function initWallet(string $uid): JsonResponse
    {
        $user = User::where('id', $uid)->first();
        if (!$user){
            return response()->json([
               'data'=>[
                   'error'=>[
                       'customer_xid'=>[
                           'Missing data for required field.'
                       ]
                   ]
               ],
                'status' =>'fail'
            ], Response::HTTP_UNAUTHORIZED);
        }

        Wallet::create(['owned_by'=>$uid]);
        return response()->json([
            'data' => [
                'token'=>md5($uid)
            ],
            'status'=> 'success'
        ], Response::HTTP_CREATED);
    }

    public function enableWallet($token):JsonResponse
    {
        $user = $this->auth($token);
        $wallet = self::findWallet($user->id);
        if ($wallet->status){
            return response()->json([
                'data'=>[
                    'error'=>[
                        'wallet'=>[
                            'Already enabled.'
                        ]
                    ]
                ],
                'status' =>'fail'
            ], Response::HTTP_BAD_REQUEST);
        }

        $wallet->update(['status', true]);

        return response()->json([
            'data' => [
                'wallet'=>$wallet
            ],
            'status'=> 'success'
        ], Response::HTTP_CREATED);
        
    }

    public function showWallet($token):JsonResponse
    {
        $user = $this->auth($token);
        $wallet = self::findWallet($user->id);
        if (!$wallet->status){
            return response()->json([
                'data'=>[
                    'error'=>[
                        'wallet'=>[
                            'Wallet disabled.'
                        ]
                    ]
                ],
                'status' =>'fail'
            ], Response::HTTP_BAD_REQUEST);
        }
        return response()->json([
            'data' => [
                'wallet'=>$wallet
            ],
            'status'=> 'success'
        ], Response::HTTP_CREATED);
    }

    public function disableWallet($token):JsonResponse
    {
        $user = $this->auth($token);
        $wallet = self::findWallet($user->id);
        if (!$wallet->status){
            return response()->json([
                'data'=>[
                    'error'=>[
                        'wallet'=>[
                            'Already disabled.'
                        ]
                    ]
                ],
                'status' =>'fail'
            ], Response::HTTP_BAD_REQUEST);
        }
        return response()->json([
            'data' => [
                'wallet'=>$wallet
            ],
            'status'=> 'success'
        ], Response::HTTP_CREATED);

    }

    public static function findWallet(string $uid):mixed
    {
        $wallet = Wallet::where('owned_by',$uid)->first();
        if (!$wallet) {
            return response()->json([
                'data' => [
                    'error' => [
                        'wallet' => [
                            'Missing data.'
                        ]
                    ]
                ],
                'status' => 'fail'
            ], Response::HTTP_BAD_REQUEST);
        }
        return $wallet;
    }
}